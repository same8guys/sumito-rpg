﻿using UnityEngine;

namespace AurynSky
{
	public class Rotator : MonoBehaviour {

		public Vector3 direction;
		public float speed;

	
		// Update is called once per frame
		private void Update () 
		{
			transform.Rotate(direction * speed * Time.deltaTime);
		}
	}
}
