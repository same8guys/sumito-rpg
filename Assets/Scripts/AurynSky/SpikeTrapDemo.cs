﻿using System.Collections;
using UnityEngine;

namespace AurynSky
{
    public class SpikeTrapDemo : MonoBehaviour {

        //This script goes on the SpikeTrap prefab;

        public Animator spikeTrapAnim; //Animator for the SpikeTrap;

        // Use this for initialization
        private void Awake()
        {
            //get the Animator component from the trap;
            spikeTrapAnim = GetComponent<Animator>();
            //start opening and closing the trap for demo purposes;
            StartCoroutine(OpenCloseTrap());
        }


        private IEnumerator OpenCloseTrap()
        {
            //play open animation;
            spikeTrapAnim.SetTrigger("open");
            //wait 2 seconds;
            yield return new WaitForSeconds(2);
            //play close animation;
            spikeTrapAnim.SetTrigger("close");
            //wait 2 seconds;
            yield return new WaitForSeconds(2);
            //Do it again;
            StartCoroutine(OpenCloseTrap());

        }
    }
}