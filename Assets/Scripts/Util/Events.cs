﻿using UnityEngine.Events;

namespace Util
{
    public class Event
    {
        [System.Serializable]
        public class EventMenuFadeComplete : UnityEvent<bool>
        {
        }

        [System.Serializable]
        public class EventGameState : UnityEvent<GameManager.GameState, GameManager.GameState>
        {
        }
    }
}
