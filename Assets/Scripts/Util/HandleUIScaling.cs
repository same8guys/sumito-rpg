using UnityEngine;
using UnityEngine.UI;

namespace Util
{
    // Switches Canvas Scaler to scale with screen size 
    // after it is rendered pixel perfect under ConstantPixelSize mode.
    // Attach it to a Canvas GameObject.
    [RequireComponent(typeof(CanvasScaler))]
    [DisallowMultipleComponent]
    public class HandleUIScaling : MonoBehaviour
    {
        void Start()
        {
            CanvasScaler scaler = GetComponent<CanvasScaler>();
            scaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
        }
    }
}