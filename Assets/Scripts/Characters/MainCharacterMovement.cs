using UnityEngine;

namespace Characters
{
    [DisallowMultipleComponent]
    public class MainCharacterMovement : MonoBehaviour
    {
        private Rigidbody rb;
        private Collider col;
        private bool isGrounded;
        public float speed = 6f;
        public float jumpSpeed = 8f;

        private void Start()
        {
            rb = GetComponent<Rigidbody>();
            col = GetComponent<CapsuleCollider>();
        }

        private void OnCollisionStay(Collision col)
        {
            // We double check to avoid buggy double jump by ensuring that the velocity is low enough when character is colliding that it makes sense to jump.
            if (col.gameObject.layer == LayerMask.NameToLayer("Ground"))
            {
                isGrounded = true;
            }
        }

        private void Update()
        {
            if (Input.GetButtonDown("Jump") && isGrounded)
            {
                rb.AddForce(Vector3.up * jumpSpeed, ForceMode.Impulse);
                isGrounded = false;
            }
        }

        private void FixedUpdate()
        {
            float horizontal = Input.GetAxisRaw("Horizontal");
            float vertical = Input.GetAxisRaw("Vertical");
            rb.velocity = new Vector3(horizontal * speed, rb.velocity.y, vertical * speed);
        }
    }
}
