﻿namespace Characters
{
    using InventoryCore;
    using ItemCore;
    using ItemCore.Items;
    using UnityEngine;
    using UnityEngine.UI;
    using Interactables;
    [DisallowMultipleComponent]
    public class Player : MonoBehaviour
    {

        public Inventory inventory;

        public Text interactText;
        
        public GameObject Target
        {
            get
            {
                return this.targetGameObject;
            }
            set
            {
                this.targetGameObject = value;
                this.targetInteractable = value?.GetComponent<IInteractable>();
                this.interactText.text = targetInteractable != null ? targetInteractable.InteractableString : "";
            }
        }
        private GameObject targetGameObject;
        private IInteractable targetInteractable;
        
        private void OnEnable()
        {
            this.interactText.gameObject.SetActive(true);
            this.interactText.text = "";

            PlayerActions.OnItemPickup += PickUpItem;
            PlayerActions.OnItemDropped += DropItem;
        }

        private void Update()
        {
            if (Input.GetButtonDown("Interact") && this.targetInteractable != null)
            {
                this.targetInteractable.Interact();
            }
        }

        /// <summary>
        /// This function is for handling OnItemPickup Events from an ItemComponent
        /// </summary>
        private void PickUpItem(ItemComponent component)
        {
            if (this.inventory.AddItem(component.item))
            {
                component.OnPickupSuccess?.Invoke();
                this.Target = null;
            }
        }

        private void DropItem(Item item)
        {
            
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Interactable") && this.Target == null)
            {
                this.Target = other.gameObject;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (this.Target == other.gameObject)
            {
                this.Target = null;
            }
        }
    }
}
