﻿namespace Characters
{
    using UnityEngine;
    [DisallowMultipleComponent]
    public class EnableShadowCasting : MonoBehaviour
    {
        private void Start()
        {
            if (this.TryGetComponent<SpriteRenderer>(out SpriteRenderer sr))
            {
                sr.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
            }
        }
    }
}
