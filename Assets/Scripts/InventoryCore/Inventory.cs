﻿using UnityEngine;
using System.Collections.Generic;
using ItemCore.Items;
using ItemCore;

namespace InventoryCore
{
    [DisallowMultipleComponent]
    public class Inventory : MonoBehaviour
    {

        private RectTransform invRect;
        private float invWidth, invHeight;
        /// <summary>
        /// The number of slots in the inventory.
        /// </summary>
        public int slots;
        public int rows;
        /// <summary> 
        /// Space between slots on the grid.
        /// </summary>
        public float slotPaddingLeft, slotPaddingTop;
        public float slotWidth, slotHeight;
        /// <summary>
        /// The inventory slots that hold items.
        /// </summary>
        public GameObject slotPrefab;
        /// <summary>
        /// The prefab for standard item drops in the game world
        /// </summary>
        public GameObject dropPrefab;
        private List<GameObject> allSlots;

        private int emptySlots;
        /// <summary>
        /// Creates the inventory slots and sets their size and layout.
        /// </summary>
        private void CreateSlots()
        {
            allSlots = new List<GameObject>();

            emptySlots = slots;

            int columns = slots / rows;
            // Calculates the width and height
            invWidth = columns * (slotWidth + slotPaddingLeft) + slotPaddingLeft;
            invHeight = rows * (slotHeight + slotPaddingTop) + slotPaddingTop;
            // Sets the size of the panel
            invRect = GetComponent<RectTransform>();
            invRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, invWidth);
            invRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, invHeight);

            for (int y = 0; y < rows; y++)
            {
                for (int x = 0; x < columns; x++)
                {
                    GameObject newSlot = (GameObject)Instantiate(slotPrefab);
                    RectTransform slotRect = newSlot.GetComponent<RectTransform>();

                    // Names the component with co-ordinates (for easier debugging)
                    newSlot.name = $"InvSlot_{x}-{y}";
                    newSlot.transform.SetParent(this.transform.parent);

                    slotRect.localPosition = invRect.localPosition + new Vector3(
                        slotPaddingLeft * (x + 1) + (slotWidth * x),
                        -slotPaddingTop * (y + 1) - (slotHeight * y)
                    );

                    slotRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, slotWidth);
                    slotRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, slotHeight);
                    allSlots.Add(newSlot);
                }
            }
        }

        private void Start()
        {
            if(this.dropPrefab == null) {
                throw new MissingReferenceException();
            } 

            CreateSlots();
            emptySlots = slots;
            PlayerActions.OnItemDropped += SpawnItemDrop;
            
        }

        private void OnDestroy() {
            PlayerActions.OnItemDropped -= SpawnItemDrop;
        }

        public void SpawnItemDrop(Item item) {
            GameObject itemDrop = Instantiate<GameObject>(dropPrefab);
            itemDrop.GetComponent<ItemComponent>().item = item;
        }

        // TODO sort open and close events
        public virtual void closeInventory()
        {
            this.gameObject.SetActive(false);
        }

        /// <summary>
        /// Makes the inventory GameObject active so it can be used
        /// </summary>
        public virtual void openInventory()
        {
            this.gameObject.SetActive(true);
        }

        /// <summary>
        /// Adds an item to the inventory if the inventory isn't full or there's a stack not at max capacity.
        /// </summary>
        /// <param name="item">The item to add to the inventory.</param>
        /// <returns>true on success</returns>
        public bool AddItem(Item item)
        {
            Debug.Log(item.name + " " + item.itemMax);
            if (item.itemMax == 1)
            {
                return PlaceEmpty(item);
            };
            return false;
        }

        private bool PlaceEmpty(Item item)
        {
            if (emptySlots > 0)
            {
                foreach (GameObject slot in allSlots)
                {
                    InventorySlot invSlot = slot.GetComponent<InventorySlot>();
                    if (invSlot.IsEmpty)
                    {
                        invSlot.AddItem(item);
                        emptySlots--;
                        return true;
                    }

                }
            }
            return false;
        }
    }
}
