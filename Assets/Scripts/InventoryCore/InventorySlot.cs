using ItemCore.Items;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

namespace InventoryCore
{
    // Class for MonoBehaviour of the prefab that will be items on the inventory UIs
    public class InventorySlot : MonoBehaviour//, IPointerEnterHandler, IPointerDownHandler, IPointerExitHandler
    {
        private Stack<Item> items;
        // text for the itemValue
        public Text stackText;
        public Image itemImage;
        public void AddItem(Item item)
        {
            this.items.Push(item);
            this.updateStackText();
            this.SetSprite(item.itemSprite);
        }

        public virtual void onItemDropped()
        {
            Item droppedItem = this.items.Pop();
            this.updateStackText();
            PlayerActions.OnItemDropped(droppedItem);
        }

        public bool IsEmpty 
        {
            get { return items.Count == 0; }
        }

        private void SetSprite(Sprite sprite)
        {
            if (sprite != null)
            {
                itemImage.sprite = sprite;
                itemImage.gameObject.SetActive(true);
            }
            else
            {
                itemImage.gameObject.SetActive(false);
            }
        }

        private void updateStackText()
        {
            int count = items.Count;
            if (count > 1)
            {
                stackText.text = count.ToString();
            }
            else
            {
                stackText.text = "";
            }
        }

        void Start()
        {
            items = new Stack<Item>();
        }
    }
}
