using System;
namespace ItemCore
{
    using Interactables;
    using ItemCore.Items;
    using UnityEngine;
    /// <summary>
    /// Interactable Items that exist to be picked up
    /// </summary>
    [RequireComponent(typeof(SpriteRenderer))]
    [DisallowMultipleComponent]
    public class ItemComponent : MonoBehaviour, IInteractable
    {
        public Item item;
        // State handling boolean to prevent duplicate calls of Interact()
        private Boolean interacting = false;

        private SpriteRenderer spriteRenderer;

        public Action OnPickupSuccess;

        public string InteractableString => "Take " + item.itemName;

        public ItemComponent(Item item)
        {
            this.item = item;
        }

        private void Start()
        {
            // If there is no item set, stop existing
            if (!this.item)
            {
                Destroy(gameObject);
                return;
            }

            // Make sprite match the set item
            spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
            spriteRenderer.sprite = this.item.itemSprite;
        }

        private void OnEnable()
        {
            OnPickupSuccess += PickupSuccess;
        }

        private void OnDisable()
        {
            OnPickupSuccess -= PickupSuccess;
        }

        /// <summary>
        /// Action taken when the item is successfully "Picked up"
        /// Made virtual in case we want special scripted events when items are picked up
        /// </summary>
        public virtual void PickupSuccess()
        {
            Destroy(this.gameObject);
        }

        /// <summary>
        /// Interaction with the item in the world.
        /// <see cref="interacting"/>
        /// </summary>
        public virtual void Interact()
        {
            // early exit to prevent duplicate invoke calls
            if (interacting)
            {
                return;
            }
            // set interacting to true so the early exit works on duplicate calls
            this.interacting = true;
            PlayerActions.OnItemPickup(this);
        }
    }
}