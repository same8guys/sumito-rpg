﻿namespace ItemCore.Items
{
    using ItemCore.Items;
    using UnityEngine;
    using static UnityEditor.AssetDatabase;

    [System.Serializable]
    [CreateAssetMenu(fileName = "New_Item", menuName = "Items/Misc")]
    public class Item : ScriptableObject, ISerializationCallbackReceiver
    {
        /// <summary>
        /// Unique ID given to each pre-defined item. 
        /// Use this to query items in an <see cref="ItemDatabaseList"/> from the <see cref="ItemDatabaseManager"/>.
        /// </summary>
        [SerializeField] private string id;

        /// <summary>
        /// Maximum size of a stack
        /// </summary>
        public int itemMax = 99;

        /// <summary>
        /// Number of this item in the stack
        /// </summary>
        public int itemCount = 1;
        public string itemName;
        [TextArea] public string itemDesc;

        /// <summary>
        /// If making a new child of the Item class, you should override this method with the proper type!
        /// </summary>
        /// <value>Item Type from enum EItemType</value>
        public virtual EItemType itemType
        {
            get
            {
                return EItemType.Misc;
            }
        }

        // Base price of the item
        public int itemPrice;

        // 2D sprite of the item
        public Sprite itemSprite;

        public string ID
        {
            get
            {
                return this.id;
            }
        }

        public Item() { }

        public Item(string id, int itemMax, string itemName, string description, int cost,
            Sprite sprite)
        {
            this.id = id;
            this.itemMax = itemMax;
            this.itemName = itemName;
            this.itemDesc = description;
            this.itemPrice = cost;
            this.itemSprite = sprite;
        }

        public virtual void Use()
        {
            Debug.Log("Some numpty didn't override this properly");
        }

        public void OnBeforeSerialize()
        {
            if (this.id == null)
            {
                this.SetGuid();
            }
        }

        public void OnAfterDeserialize() { }

        private void SetGuid()
        {
            if (Application.isPlaying)
            {
                return;
            }

            this.id = AssetPathToGUID(GetAssetPath(this));
        }

        public Item Clone()
        {
            return this.MemberwiseClone() as Item;
        }

        /// <summary>
        /// Use the item, different implementation for different types
        /// </summary>
        public virtual void UseItem()
        {

        }
    }
}
