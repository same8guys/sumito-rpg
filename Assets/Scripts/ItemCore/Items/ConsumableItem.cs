﻿namespace ItemCore.Items
{
    using UnityEngine;

    [CreateAssetMenu(fileName = "New_Item", menuName = "Items/Consumable", order = 1)]
    public class ConsumableItem : Item
    {
        public override EItemType itemType
        {
            get
            {
                return EItemType.Consumable;
            }
        }
        public override void UseItem()
        {
            Debug.Log("Overridden Use() Function");
            base.UseItem();
        }
    }
}
