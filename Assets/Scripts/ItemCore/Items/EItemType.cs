﻿namespace ItemCore.Items
{
    /// <summary>
    /// Enum to determine the "type" of an item.
    /// Unique items that cannot be dropped are Key Items.
    /// Junk made for the memes with no use other than to sell is a Misc Item.
    /// </summary>
    public enum EItemType
    {
        Misc, // Generic mob drops for sell or quests
        Consumable, // Items with battle or field effects
        Weapon, // Chainsaws and Crowbars
        Armor, // Only the latest drip
        Key //Use this for unique quest items too
    }

/// <summary>
/// This could be changed later but for now
/// is an enum to determine which slot to equip and item
/// </summary>
    public enum EEquipSlot
    {
        Head,
        Chest,
        Legs,
        Feet,
        Hands,
        Accessory,
        OneHanded,
        TwoHanded
    }
}
