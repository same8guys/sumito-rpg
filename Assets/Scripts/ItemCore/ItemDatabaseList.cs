﻿namespace ItemCore
{
    using System.Collections.Generic;
    using Items;
    using UnityEngine;

    [System.Serializable]
    [CreateAssetMenu(fileName = "New_Item_Database", menuName = "Databases/ItemDatabase")]
    public class ItemDatabaseList : ScriptableObject
    {
        public List<Item> Items { get; set; }
    }
}
