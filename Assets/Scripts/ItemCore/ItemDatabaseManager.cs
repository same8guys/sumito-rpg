namespace ItemCore
{
    using System.Linq;
    using Items;
    using UnityEngine;
    using Util;

    public class ItemDatabaseManager : Singleton<ItemDatabaseManager>
    {
        // I may give equipment a separate database instance object, not sure yet
        // This class will be used later for creating loot tables, and will require some changes
        public ItemDatabaseList itemDB;

        private void Start()
        {
            this.itemDB.Items = Resources.LoadAll<Item>("Items/Misc").ToList();
        }

        public Item GETItemByID(string id)
        {
            return this.itemDB.Items.FirstOrDefault(item => item.ID == id);
        }

        // TODO make function to get a random item
    }
}
