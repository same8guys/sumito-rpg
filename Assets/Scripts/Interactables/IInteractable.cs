namespace Interactables
{
    public interface IInteractable
    {
        /// <value>The string to display when entering the collider.</value>
        string InteractableString { get; }
        void Interact();
    }
}
