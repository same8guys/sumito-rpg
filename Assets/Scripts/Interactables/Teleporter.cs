using UnityEngine;
using UnityEngine.SceneManagement;

namespace Interactables
{
    [DisallowMultipleComponent]
    public class Teleporter : MonoBehaviour, IInteractable
    {
        public string sceneToLoad;

        public float x, y, z;

        public string InteractableString => "Go to " + sceneToLoad;

        public void Interact()
        {
            string oldScene = SceneManager.GetActiveScene().name;
            GameManager gameManager = GameManager.Instance;
            gameManager.LoadScene(sceneToLoad);
            Debug.Log("Player is switching to level: " + sceneToLoad);

        }
    }
}