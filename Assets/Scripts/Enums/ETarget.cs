﻿namespace Enums
{
    /**
     * Enum for ability/item targets
     */
    public enum ETarget
    {
        Self,
        Enemy,
        EnemyTeam,
        Ally,
        AllyTeam
    }
}
