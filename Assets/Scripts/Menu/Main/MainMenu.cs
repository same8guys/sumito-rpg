﻿namespace Menu.Main
{
    using UnityEngine;
    using UnityEngine.Serialization;
    using Event = Util.Event;

    public class MainMenu : MonoBehaviour
    {
        [FormerlySerializedAs("_mainMenuAnimator")] [SerializeField]
        private Animation mainMenuAnimator;

        [FormerlySerializedAs("_fadeOutAnimation")] [SerializeField]
        private AnimationClip fadeOutAnimation;

        [FormerlySerializedAs("_fadeInAnimation")] [SerializeField]
        private AnimationClip fadeInAnimation;

        [FormerlySerializedAs("OnMainMenuFadeComplete")]
        public Event.EventMenuFadeComplete onMainMenuFadeComplete;

        private void Start() => GameManager.Instance.onGameStateChanged.AddListener(this.HandleGameStateChanged);

        private void HandleGameStateChanged(GameManager.GameState current, GameManager.GameState previous)
        {
            if (previous == GameManager.GameState.Pregame && current == GameManager.GameState.Running)
            {
                this.FadeOut();
            }

            if (previous != GameManager.GameState.Pregame && current == GameManager.GameState.Pregame)
            {
                this.FadeIn();
            }
        }

        public void OnFadeOutComplete() => this.onMainMenuFadeComplete.Invoke(true);

        public void OnFadeInComplete()
        {
            this.onMainMenuFadeComplete.Invoke(false);
            UIManager.Instance.SetDummyCameraActive(true);
        }

        public void FadeIn()
        {
            this.mainMenuAnimator.Stop();
            this.mainMenuAnimator.clip = this.fadeInAnimation;
            this.mainMenuAnimator.Play();
        }

        public void FadeOut()
        {
            UIManager.Instance.SetDummyCameraActive(false);

            this.mainMenuAnimator.Stop();
            this.mainMenuAnimator.clip = this.fadeOutAnimation;
            this.mainMenuAnimator.Play();
        }
    }
}
