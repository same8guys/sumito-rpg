﻿namespace Menu.Pause
{
    using UnityEngine;
    using UnityEngine.Serialization;
    using UnityEngine.UI;

    public class PauseMenu : MonoBehaviour
    {
        [FormerlySerializedAs("ResumeButton")] public Button resumeButton;
        [FormerlySerializedAs("QuitButton")] public Button quitButton;

        [FormerlySerializedAs("RestartButton")]
        public Button restartButton;

        private void Awake()
        {
            this.resumeButton.onClick.AddListener(this.HandleResumeClick);
            this.quitButton.onClick.AddListener(this.HandleQuitClick);
            this.restartButton.onClick.AddListener(this.HandleRestartClick);
        }

        private void HandleResumeClick() => GameManager.Instance.TogglePause();

        private void HandleQuitClick() => GameManager.Instance.QuitGame();

        private void HandleRestartClick() => GameManager.Instance.RestartGame();
    }
}
