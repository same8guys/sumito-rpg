namespace Menu
{
    using Main;
    using Pause;
    using UnityEngine;
    using UnityEngine.Serialization;
    using Util;
    using Event = Util.Event;
    using InventoryCore;

    public class UIManager : Singleton<UIManager>
    {
        // GameObjects
        public MainMenu mainMenu;
        public PauseMenu pauseMenu;

        public Inventory inventoryMenu;
        public Camera dummyCamera;

        public Event.EventMenuFadeComplete onMainMenuFadeComplete;

        private void Start()
        {
            DontDestroyOnLoad(this.gameObject);

            this.mainMenu.onMainMenuFadeComplete.AddListener(this.HandleMainMenuFadeComplete);
            GameManager.Instance.onGameStateChanged.AddListener(this.HandleGameStateChanged);
        }

        private void Update()
        {
            if (GameManager.Instance.CurrentGameState != GameManager.GameState.Pregame)
            {
                return;
            }

            if (Input.anyKeyDown)
            {
                GameManager.Instance.StartGame();
            }
        }

        private void HandleMainMenuFadeComplete(bool fadeOut)
        {
            this.onMainMenuFadeComplete.Invoke(fadeOut);
        }
        private void HandleGameStateChanged(GameManager.GameState current, GameManager.GameState previous)
        {
            this.pauseMenu.gameObject.SetActive(current == GameManager.GameState.Paused);
        }
        public void SetDummyCameraActive(bool active)
        {
            this.dummyCamera.gameObject.SetActive(active);
        }
    }
}
