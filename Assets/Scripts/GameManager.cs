using System.Globalization;
using System.Collections.Generic;
using Menu;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using Util;
using Event = Util.Event;

public class GameManager : Singleton<GameManager>
{
    // Scene to start the game on
    [SerializeField] private string firstScene = "DevTest_Scene";

    // PREGAME, RUNNING PAUSED
    public enum GameState
    {
        Pregame,
        Running,
        Paused
    }

    private GameState currentGameState = GameState.Pregame;
    private List<AsyncOperation> loadOperations;

    // Store prefabs needed by Game Manager here (e.g. Inventory, Audio, Enemy managers)
    public GameObject[] systemPrefabs;

    public Event.EventGameState onGameStateChanged;

    private List<GameObject> instancedSystemPrefabs;

    public GameState CurrentGameState
    {
        get => this.currentGameState;
        private set => this.currentGameState = value;
    }

    private void Start()
    {
        // Exclude object from cleanup list so it is never destroyed
        DontDestroyOnLoad(this.gameObject);

        this.instancedSystemPrefabs = new List<GameObject>();
        this.loadOperations = new List<AsyncOperation>();

        this.InstantiateSystemPrefabs();

        UIManager.Instance.onMainMenuFadeComplete.AddListener(this.HandleMainMenuFadeComplete);
    }

    private void HandleMainMenuFadeComplete(bool fadeOut)
    {
        if (!fadeOut)
        {
            this.UnloadScene(this.currentSceneName);
        }
    }

    private void Update()
    {
        if (Instance.CurrentGameState == GameState.Pregame)
        {
            return;
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            this.TogglePause();
        }
    }

    // What scene the game is on
    private string currentSceneName = string.Empty;

    // Load and unload scene
    public void LoadScene(string sceneName)
    {
        AsyncOperation ao = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
        if (ao == null)
        {
            Debug.LogError("[GameManager] Unable to load scene " + sceneName);
            return;
        }

        ao.completed += this.OnLoadOperationComplete;
        this.loadOperations.Add(ao);
        this.currentSceneName = sceneName;
    }

    private void OnLoadOperationComplete(AsyncOperation ao)
    {

        if (SceneManager.GetActiveScene().name != this.currentSceneName)
        {
            SceneManager.SetActiveScene(SceneManager.GetSceneByName(this.currentSceneName));
        }

        if (this.loadOperations.Contains(ao))
        {
            this.loadOperations.Remove(ao);

            if (this.loadOperations.Count == 0)
            {
                this.UpdateState(GameState.Running);
            }
        }

        Debug.Log(message: "Load Complete.");
    }

    public void UnloadScene(string sceneName)
    {
        AsyncOperation ao = SceneManager.UnloadSceneAsync(sceneName);
        if (ao == null)
        {
            Debug.LogError("[GameManager] Unable to unload scene " + sceneName);
            return;
        }

        ao.completed += this.OnUnloadOperationComplete;
    }

    private void OnUnloadOperationComplete(AsyncOperation ao) { }

    private void UpdateState(GameState state)
    {
        GameState previousGameState = this.currentGameState;
        this.currentGameState = state;

        switch (this.currentGameState)
        {
            case GameState.Pregame:
                Time.timeScale = 1.0f;
                break;
            case GameState.Running:
                Time.timeScale = 1.0f;
                break;
            case GameState.Paused:
                Time.timeScale = 0.0f;
                break;

            default:
                break;
        }

        this.onGameStateChanged.Invoke(this.currentGameState, previousGameState);
    }

    // generate other persistent systems/managers
    public void InstantiateSystemPrefabs()
    {
        GameObject prefabInstance;
        foreach (GameObject t in this.systemPrefabs)
        {
            prefabInstance = Instantiate(t);
            this.instancedSystemPrefabs.Add(prefabInstance);
        }
    }

    // Clean up systems before destroying self
    protected override void OnDestroy()
    {
        base.OnDestroy();
        foreach (GameObject t in this.instancedSystemPrefabs)
        {
            Destroy(t);
        }

        this.instancedSystemPrefabs.Clear();
    }

    public void StartGame()
    {
        this.LoadScene(this.firstScene);
    }

    public void TogglePause()
    {
        this.UpdateState(this.currentGameState == GameState.Running ? GameState.Paused : GameState.Running);
    }

    public void QuitGame()
    {
        // TODO Implement save on exit
        Application.Quit();
    }


    public void RestartGame()
    {
        this.UpdateState(GameState.Pregame);
    }
}
