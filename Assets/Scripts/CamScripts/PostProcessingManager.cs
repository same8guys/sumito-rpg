using System.Collections.Generic;
namespace CamScripts
{
    using Event = Util.Event;
    using Util;
    using UnityEngine.Rendering.PostProcessing;
    using UnityEngine;

    [RequireComponent(typeof(PostProcessVolume))]
    public class PostProcessingManager : Singleton<PostProcessingManager>
    {
        /// <summary>
        /// ( ͡° ͜ʖ ͡°)
        /// </summary>
        public PostProcessVolume ppVolume;

        public List<PostProcessProfile> profiles;

        private void Start()
        {
            DontDestroyOnLoad(this.gameObject);

            this.ppVolume = GetComponent<PostProcessVolume>();
        }
    }
}
